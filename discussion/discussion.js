//Aggregation in MongdDb and Query Case Studies

db.fruits.insertMany([
    {
        name : "Apple",
        color : "Red",
        stock : 20,
        price: 40,
        supplier_id : 1,
        onSale : true,
        origin: [ "Philippines", "US" ]
    },

    {
        name : "Banana",
        color : "Yellow",
        stock : 15,
        price: 20,
        supplier_id : 2,
        onSale : true,
        origin: [ "Philippines", "Ecuador" ]
    },

    {
        name : "Kiwi",
        color : "Green",
        stock : 25,
        price: 50,
        supplier_id : 1,
        onSale : true,
        origin: [ "US", "China" ]
    },

    {
        name : "Mango",
        color : "Yellow",
        stock : 10,
        price: 120,
        supplier_id : 2,
        onSale : false,
        origin: [ "Philippines", "India" ]
    }     	
])


// Aggregation Pipeline

/*
        Documentation on Aggregation
        - https://www.mongodb.com/docs/manual/reference/operator/aggregation-pipeline/


        1st Phase - $match is responsible for gathering the initial items base on a certain argument/criteria
        2nd Phase - $group phase is responsible for grouping the specific fields from the documents after the    criteria has been determined.




*/


// 1 Stage only 
// .count() - is for counting an object or specific elements or fields

db.fruits.count();


// the $count stage returns a count of the remaining documents in the aggregation pipeline and assigns the value to a field.

db.fruits.aggregate([

    {
        $match: {onSale: true}
    },
    { $count: "fruits"}   //counts the group or field,, "fruits" can choose any name
])

// 2nd Stage

// match and $group

db.fruits.aggregate ([
    {
        $match: { onSale: true }

    },
    {
        $group: { 
            _id: "supplier_id", //example if "$supplier_id" therefore group mugawas, all supplier_id groupings will add, result = all supplier_id - 1 will add all , supplier_id - 2 add pd mga naay
            totalStocks: { $sum: "$stock"}
        }

    }

])


//3rd Phase - OPTIONAL - $project phase is responsible for excluding certain fields that do not need to show up in the final result


//3RD PHASE
db.fruits.aggregate ([
    {
        $match: { onSale: true }

    },
    {
        $group: { 
            _id: "$supplier_id",
            totalStocks: { $sum: "$stock"},
            price: {$sum: "$price"}
        }

    },
    {
        $project: { 
            
            totalStocks: 1,//1 = inclusion, 0 = exclusion
            _id: 0,
            totalStocks: { $gte: [ "$stocks", 10 ] }
                 }
                }
                 ,
       {$sort: {   
            price: 1
        }
    }

    

])


//$sort operator - responsible for sorting/arranging items based on their value.(1 means ascending, -1 means descending)

// $unwind operator - responsible for deconstructuring an array and using them as the unique identifiers for each row in the result.

db.fruits.aggregate([
    {$unwind: "$origin"},
    {$project: 
        {
            origin: 1,
            name: 1,
            onSale: 1
        }
    }

]);

//$MATCH ADDED
db.fruits.aggregate([
    {$unwind: "$origin"},
    {$project: 
        {
            origin: 1,
            name: 1,
            onSale: 1
        }
    },
    {$match: {onSale: true} }// pwede butang condition

]);

//$GROUP ADDED
db.fruits.aggregate([
    {$unwind: "$origin"},
   
    {$group: { _id : "origin", kinds : { $sum : 1 } }}

]);

















//ACTIVITY

db.fruits.aggregate ([
    {
        $match: { onSale: true }

    },
    {
        $group: { 
            _id: "$supplier_id",
            totalStocks: { $sum: "$stock"},
            price: {$sum: "$price"}
        }

    },
    {
        $project: { 
            
            totalStocks: 1,
            _id: 0,
            totalStocks: { $gte: [ "$stocks", 10 ] }



                 }
                }
                 ,
       {$sort: {   
            price: 1
        }
    }
])




db.fruits.aggregate ([
    {
        $match: { onSale: true }

    },
    {
        $group: { 
            _id: "$supplier_id",
            totalStocks: { $max: "$stock"}
        }

    }

])