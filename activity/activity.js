//ACTIVITY
//1
db.fruits.aggregate([

    { $match: {onSale: true} },
    { $count: "fruits"} ]);

//2
db.fruits.aggregate([
     {$match: {"stock": {$gte: 20}}},
      { $project:{
            item: 1,
            "enoughStock": {"$size": "$stock"},
             _id: 0
                 }
      }
    ]);



//3

db.fruits.aggregate ([
    {$match: { onSale: true }},
    {
        $group: { 
            _id: "$supplier_id",
            totalStocks: { $sum: "$stock"},
            price: {$avg: "$price"}
                }

    },
    { $project: { 
             _id: 1,
            price: 1
                } 
    },
                 
    { $sort: {   
            price: -1
             }
    }]);


//4

db.fruits.aggregate ([
    { $match: { onSale: true } },
    {$group: { 
            _id: "$supplier_id",
            maxPrice: { $max: "$stock"}
             }
    }]);



//5
db.fruits.aggregate ([
    { $match: { onSale: true } },
    {$group: { 
            _id: "$supplier_id",
            minPrice: { $min: "$stock"}
             }
    }]);
